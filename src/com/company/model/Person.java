

/**
 * Created by Jonatan Delgado on 11/9/2018.
 */

public class Person {
    private String name;
    private String lastName;
    private int age;

    public Person(String name,String lastName, int age)
    {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public String name_and_lastname()
    {
        if(this.getAge() > 0 && this.getAge() < 110 ){
            return this.getName() +" "+ getLastName();
        }
        return "The person not exists.";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
