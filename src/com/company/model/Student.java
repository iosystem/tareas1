package com.company.model;

/**
 * Created by Jonatan Delgado on 11/9/2018.
 */

public class Student {
    private int semester;
    private String matter;
    private double qualification;

    public Student(int semester,String matter,double qualification)
    {
        this.semester = semester;
        this.matter = matter;
        this.qualification = qualification;
    }


    public double getQualification() throws Exception {
        if(this.qualification > 0 && this.qualification < 100 ) {
            return this.qualification;
        }
        throw   new Exception("La calificacion que se tiene no es valida.");
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public String getMatter() {
        return matter;
    }

    public void setMatter(String matter) {
        this.matter = matter;
    }

    public void setQualification(double qualification) {
        this.qualification = qualification;
    }
}
