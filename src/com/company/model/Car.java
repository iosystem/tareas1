package com.company.model;

import com.example.jonatandelgado.appproductos.Interface.Vehicle;

/**
 * Created by Jonatan Delgado on 11/9/2018.
 */

public class Car implements Vehicle {
    private String color;
    private String tipomotor;
    private String tipo_combustible;
    private String yieldPerGallon;
    private String description;

    public Car(String color,String tipomotor,String tipo_combustible,String yieldPerGallon,String description)
    {
        this.color = color;
        this.tipomotor=tipomotor;
        this.tipo_combustible=tipo_combustible;
        this.yieldPerGallon=yieldPerGallon;
        this.description=description;
    }
    @Override
    public void startCar() {

    }

    @Override
    public void brake() {

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipomotor() {
        return tipomotor;
    }

    public void setTipomotor(String tipomotor) {
        this.tipomotor = tipomotor;
    }

    public String getTipo_combustible() {
        return tipo_combustible;
    }

    public void setTipo_combustible(String tipo_combustible) {
        this.tipo_combustible = tipo_combustible;
    }

    public String getYieldPerGallon() {
        return yieldPerGallon;
    }

    public void setYieldPerGallon(String rendimientoxgalon) {
        this.yieldPerGallon = rendimientoxgalon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void print(){

        System.out.println("Datos del Carro: ");
        System.out.println("Marca: "+this.mark);
        System.out.println("Modelo: "+this.name);
        System.out.println("Año: "+this.year);
        System.out.println("Tipo de motor: "+getTipomotor());
        System.out.println("Rendimiento por galon: "+getYieldPerGallon());

    }

}
