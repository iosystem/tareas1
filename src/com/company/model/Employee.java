

import java.util.Date;

/**
 * Created by Jonatan Delgado on 11/9/2018.
 */

public class Employee {
    private int id_employee;
    private double salary;
    private String position;
    private String civil_status;
    private Date admission_date;
    private boolean active;

    public  Employee(int id_employee,double salary,String position,String civil_status,Date admission_date,boolean active)
    {
        this.id_employee = id_employee;
        this.salary = salary;
        this.position = position;
        this.civil_status =civil_status;
        this.admission_date =admission_date;
        this.active =active;

    }

    public int getId_employee() {
        return id_employee;
    }

    public void setId_employee(int id_employee) {
        this.id_employee = id_employee;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCivil_status() {
        return civil_status;
    }

    public void setCivil_status(String civil_status) {
        this.civil_status = civil_status;
    }

    public Date getAdmission_date() {
        return admission_date;
    }

    public void setAdmission_date(Date admission_date) {
        this.admission_date = admission_date;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void seeCode(int code)
    {

    }
}
