package com.company.model;

import java.util.Date;

/**
 * Created by Jonatan Delgado on 11/9/2018.
 */

public class Bird {
    private int id_ave;
    private String Photo;
    private String first_name;
    private String species;
    private String description;
    private boolean  _native;
    private String place_endermico;
    private String place_of_emigration;
    private Date registration_date;
    private boolean active;

    public Bird(int id_ave, String photo, String first_name, String species, String description, boolean _native, String place_endermico, String place_of_emigration, Date registration_date, boolean active) {
        this.id_ave = id_ave;
        Photo = photo;
        this.first_name = first_name;
        this.species = species;
        this.description = description;
        this._native = _native;
        this.place_endermico = place_endermico;
        this.place_of_emigration = place_of_emigration;
        this.registration_date = registration_date;
        this.active = active;
    }

    public int getId_ave() {
        return id_ave;
    }

    public void setId_ave(int id_ave) {
        this.id_ave = id_ave;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean is_native() {
        return _native;
    }

    public void set_native(boolean _native) {
        this._native = _native;
    }

    public String getPlace_endermico() {
        return place_endermico;
    }

    public void setPlace_endermico(String place_endermico) {
        this.place_endermico = place_endermico;
    }

    public String getPlace_of_emigration() {
        return place_of_emigration;
    }

    public void setPlace_of_emigration(String place_of_emigration) {
        this.place_of_emigration = place_of_emigration;
    }

    public Date getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(Date registration_date) {
        this.registration_date = registration_date;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
