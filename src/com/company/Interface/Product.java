package com.company.Interface;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Jonatan Delgado on 11/9/2018.
 */

 public abstract class Product {
    private int codigo;
    private String nombre;
    private Double precio;
    private int Cantidad;
    private int cantidad_reorden;
    private int idcategoria;
    private Date fecha_ingreso;
    private boolean activo;

    public abstract ArrayList<Object> productos_activos();

    public abstract ArrayList<Object> productos_inactivos();

    public abstract void buscar_cantidad_de_reorden();

   public int getCodigo() {
      return codigo;
   }

   public void setCodigo(int codigo) {
      this.codigo = codigo;
   }

   public String getNombre() {
      return nombre;
   }

   public void setNombre(String nombre) {
      this.nombre = nombre;
   }

   public Double getPrecio() {
      return precio;
   }

   public void setPrecio(Double precio) {
      this.precio = precio;
   }

   public int getCantidad() {
      return Cantidad;
   }

   public void setCantidad(int cantidad) {
      Cantidad = cantidad;
   }

   public int getCantidad_reorden() {
      return cantidad_reorden;
   }

   public void setCantidad_reorden(int cantidad_reorden) {
      this.cantidad_reorden = cantidad_reorden;
   }

   public int getIdcategoria() {
      return idcategoria;
   }

   public void setIdcategoria(int idcategoria) {
      this.idcategoria = idcategoria;
   }

   public Date getFecha_ingreso() {
      return fecha_ingreso;
   }

   public void setFecha_ingreso(Date fecha_ingreso) {
      this.fecha_ingreso = fecha_ingreso;
   }

   public boolean isActivo() {
      return activo;
   }

   public void setActivo(boolean activo) {
      this.activo = activo;
   }
}
