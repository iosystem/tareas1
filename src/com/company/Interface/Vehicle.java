package com.example.jonatandelgado.appproductos.Interface;

/**
 * Created by Jonatan Delgado on 11/9/2018.
 */

public interface Vehicle {
    String name = "Corola";
    String mark = "Toyota";
    int year = 2018;
    void startCar();
    void brake();
}
